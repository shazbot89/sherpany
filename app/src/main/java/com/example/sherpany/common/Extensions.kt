package com.example.sherpany.common

import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

/**
 * Convenience extension to subscribe and observe to an [Observable] on [Schedulers.io]
 */
fun <T> Observable<T>.onBackground(): Observable<T> =
    this.subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())

/**
 * Convenience extension to subscribe and observe to a [Completable] on [Schedulers.io]
 */
fun Completable.onBackground(): Completable =
    this.subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())

/**
 * Convenience extension to add dividers to [RecyclerView].
 */
fun RecyclerView.addDividers() = addItemDecoration(
    DividerItemDecoration(this.context, DividerItemDecoration.VERTICAL)
)
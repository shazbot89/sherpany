package com.example.sherpany.common

/**
 * Interface for classes that implement searching.
 */
interface Searchable {

    fun searchFor(query: String?)

}

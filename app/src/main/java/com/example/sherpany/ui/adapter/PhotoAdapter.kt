package com.example.sherpany.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sherpany.bind.BindableAdapter
import com.example.sherpany.databinding.ItemAlbumPhotoBinding
import com.example.sherpany.model.Photo
import com.example.sherpany.model.Photos

/**
 * [RecyclerView.Adapter] for [Photos].
 */
class PhotoAdapter : RecyclerView.Adapter<PhotoAdapter.ViewHolder>(), BindableAdapter<Photos> {

    var photos = emptyList<Photo>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemAlbumPhotoBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(photos[position])

    override fun getItemCount(): Int = photos.size

    override fun getItemId(position: Int): Long = photos[position].id

    class ViewHolder(private val binding: ItemAlbumPhotoBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(photo: Photo) {
            with(binding) {
                photoUrl = photo.url
                executePendingBindings()
            }
        }
    }

    override fun setData(data: Photos?) {
        photos = data ?: emptyList()
        notifyDataSetChanged()
    }

}
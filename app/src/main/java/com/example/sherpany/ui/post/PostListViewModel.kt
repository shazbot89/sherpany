package com.example.sherpany.ui.post

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.example.sherpany.common.Event
import com.example.sherpany.common.Searchable
import com.example.sherpany.db.DisplayPosts
import com.example.sherpany.repo.PostRepository

class PostListViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    private val repository: PostRepository
) : ViewModel(), Searchable {

    private val search = MutableLiveData("")

    private val allPosts: LiveData<DisplayPosts> = repository.posts

    val posts: LiveData<DisplayPosts>
        get() = Transformations.switchMap(search) { query ->
            when (query) {
                "" -> allPosts
                else -> {
                    val result = allPosts.value?.filter { post -> post.title.contains(query) }
                    MutableLiveData<DisplayPosts>(result)
                }
            }
        }

    val onPostClick: MutableLiveData<Event<Long>> = MutableLiveData()

    fun deletePost(id: Long) = repository.deletePost(id)

    fun showPost(id: Long) {
        onPostClick.value = Event(id)
    }

    override fun searchFor(query: String?) {
        search.value = query
    }

}
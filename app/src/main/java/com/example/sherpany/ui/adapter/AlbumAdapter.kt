package com.example.sherpany.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sherpany.bind.BindableAdapter
import com.example.sherpany.databinding.ItemAlbumBinding
import com.example.sherpany.db.DisplayAlbum
import com.example.sherpany.db.DisplayAlbums

/**
 * [RecyclerView.Adapter] for [DisplayAlbums].
 */
class AlbumAdapter(
    val context: Context
) : RecyclerView.Adapter<AlbumAdapter.ViewHolder>(),
    BindableAdapter<DisplayAlbums> {

    private var displayAlbums = emptyList<DisplayAlbum>()

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemAlbumBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(displayAlbums[position])

    override fun getItemCount(): Int = displayAlbums.size

    override fun getItemId(position: Int): Long = displayAlbums[position].album.id

    class ViewHolder(private val binding: ItemAlbumBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(displayAlbum: DisplayAlbum) {
            with(binding) {
                album = displayAlbum
                photoAdapter = PhotoAdapter()
                executePendingBindings()
            }
        }
    }

    override fun setData(data: DisplayAlbums?) {
        displayAlbums = data ?: emptyList()
        notifyDataSetChanged()
    }

}


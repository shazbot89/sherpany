package com.example.sherpany.ui.post

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.example.sherpany.db.DisplayAlbums
import com.example.sherpany.db.PostDetails
import com.example.sherpany.repo.PostRepository

class PostDetailViewModel @ViewModelInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    private val repository: PostRepository
) : ViewModel() {

    /**
     * Lazily get the id of the first post
     */
    private val defaultPostId: LiveData<Long> by lazy {
        repository.posts.map { it.first().id }
    }

    /**
     * The post id selected by user
     */
    private val selectedId: LiveData<Long?> =
        MutableLiveData(savedStateHandle.get<Long?>(ARG_POST_ID))

    /**
     * The post id selected by the user, defaults to [defaultPostId] when null
     */
    private val postId: LiveData<Long> =
        Transformations.switchMap(selectedId) { id ->
            when (id) {
                null -> defaultPostId
                else -> MutableLiveData(id)
            }
        }

    val postDetails: LiveData<PostDetails> =
        Transformations.switchMap(postId) { repository.getPostDetails(it) }

    val albums: LiveData<DisplayAlbums> =
        Transformations.switchMap(postId) { repository.getPostAlbums(it) }

}

package com.example.sherpany.ui.post

import androidx.lifecycle.SavedStateHandle

/**
 * Apparently [SavedStateHandle] can access a Fragment's SafeArgs, so using this key to match
 * where is in the `nav_graph.xml`.
 */
const val ARG_POST_ID = "postId"
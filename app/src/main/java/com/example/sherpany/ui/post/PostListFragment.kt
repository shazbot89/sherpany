package com.example.sherpany.ui.post

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.sherpany.R
import com.example.sherpany.common.addDividers
import com.example.sherpany.databinding.FragmentPostListBinding
import com.example.sherpany.ui.adapter.PostAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_post_list.view.*

/**
 * A fragment representing a list of Items.
 */
@AndroidEntryPoint
class PostListFragment : Fragment() {

    private val viewModel: PostListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = FragmentPostListBinding.inflate(inflater, container, false)
        .apply {
            lifecycleOwner = viewLifecycleOwner
            adapter = PostAdapter(viewModel)
            vm = viewModel
        }
        .root
        .also { it.post_list.addDividers() }

    override fun onStart() {
        super.onStart()
        observeNavigationEvent()
    }

    private fun observeNavigationEvent() =
        viewModel.onPostClick.observe(viewLifecycleOwner) { event ->
            event.getContentIfNotHandled()?.let { postId ->
                val showPostDetails = PostListFragmentDirections.actionShowPostDetails(postId)
                findNavController().navigate(showPostDetails)
            }
        }

    private val queryTextListener = object : SearchView.OnQueryTextListener {

        override fun onQueryTextSubmit(query: String?): Boolean = true

        override fun onQueryTextChange(query: String?): Boolean {
            viewModel.searchFor(query)
            return true
        }

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_searchable, menu)
        val searchItem = menu.findItem(R.id.action_search)
        val searchView = searchItem.actionView as? SearchView ?: return
        searchView.setOnQueryTextListener(queryTextListener)
    }
}
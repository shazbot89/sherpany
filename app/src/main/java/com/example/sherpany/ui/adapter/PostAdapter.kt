package com.example.sherpany.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sherpany.bind.BindableAdapter
import com.example.sherpany.databinding.ItemPostBinding
import com.example.sherpany.db.DisplayPost
import com.example.sherpany.db.DisplayPosts
import com.example.sherpany.ui.post.PostListViewModel

/**
 * [RecyclerView.Adapter] for [DisplayPosts].
 */
class PostAdapter(private val listViewModel: PostListViewModel) :
    RecyclerView.Adapter<PostAdapter.ViewHolder>(),
    BindableAdapter<DisplayPosts> {

    private var displayPosts = emptyList<DisplayPost>()

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemPostBinding.inflate(inflater, parent, false)
            .apply { adapter = this@PostAdapter }
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(displayPosts[position])

    override fun getItemCount(): Int = displayPosts.size

    override fun getItemId(position: Int): Long = displayPosts[position].id

    fun onItemSelected(postId: Long) = listViewModel.showPost(postId)

    fun removePost(post: DisplayPost) {
        listViewModel.deletePost(post.id)
        val position = displayPosts.indexOf(post)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, itemCount)
    }

    class ViewHolder(private val binding: ItemPostBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(displayPost: DisplayPost) {
            with(binding) {
                post = displayPost
                executePendingBindings()
            }
        }
    }

    override fun setData(data: DisplayPosts?) {
        displayPosts = data ?: emptyList()
        notifyDataSetChanged()
    }
}
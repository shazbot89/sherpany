package com.example.sherpany.ui.post

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.sherpany.databinding.FragmentPostDetailBinding
import com.example.sherpany.ui.adapter.AlbumAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PostDetailFragment : Fragment() {

    private val viewModel: PostDetailViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = FragmentPostDetailBinding.inflate(inflater, container, false)
        .apply {
            lifecycleOwner = viewLifecycleOwner
            vm = viewModel
            adapter = AlbumAdapter(requireContext())
        }
        .root
}
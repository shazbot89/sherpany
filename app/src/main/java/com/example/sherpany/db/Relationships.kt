package com.example.sherpany.db

import androidx.room.Embedded
import androidx.room.Relation
import com.example.sherpany.model.*

data class UserPosts(
    @Embedded val user: User,
    @Relation(
        parentColumn = "id",
        entityColumn = "userId"
    )
    val posts: Posts
)

data class UserAlbums(
    @Embedded val user: User,
    @Relation(
        parentColumn = "id",
        entityColumn = "userId"
    )
    val albums: Albums
)

data class AlbumPhotos(
    @Embedded val album: Album,
    @Relation(
        parentColumn = "id",
        entityColumn = "albumId"
    )
    val photos: Photos
)
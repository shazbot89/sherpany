package com.example.sherpany.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.sherpany.model.Albums
import com.example.sherpany.model.Photos
import com.example.sherpany.model.Posts
import com.example.sherpany.model.Users

@Dao
interface PostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUsers(users: Users)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAlbums(albums: Albums)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPosts(posts: Posts)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPhotos(photos: Photos)

    @Query("SELECT * FROM displayPosts")
    fun getDisplayPosts(): LiveData<DisplayPosts>

    @Transaction
    @Query("SELECT * FROM albums WHERE albums.id = (:postId)")
    fun getPostAlbums(postId: Long): LiveData<DisplayAlbums>

    @Query("SELECT * FROM postDetails WHERE id = (:postId)")
    fun getPostDetails(postId: Long): LiveData<PostDetails>

    @Query("DELETE FROM posts WHERE id IN (:postId)")
    fun deletePostById(vararg postId: Long)

}
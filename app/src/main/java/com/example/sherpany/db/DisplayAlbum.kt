package com.example.sherpany.db

import androidx.room.Embedded
import androidx.room.Relation
import com.example.sherpany.model.Album
import com.example.sherpany.model.Photos

typealias DisplayAlbums = List<DisplayAlbum>

data class DisplayAlbum(
    @Embedded val album: Album,
    @Relation(
        parentColumn = "id",
        entityColumn = "albumId"
    )
    val photos: Photos
)
package com.example.sherpany.db

import androidx.room.DatabaseView

@DatabaseView(
    value = """SELECT posts.id, posts.title, posts.body FROM posts""",
    viewName = "postDetails"
)
data class PostDetails(
    val id: Long,
    val title: String,
    val body: String
)
package com.example.sherpany.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.sherpany.model.Album
import com.example.sherpany.model.Photo
import com.example.sherpany.model.Post
import com.example.sherpany.model.User

/**
 * This is my first time using [androidx.room.Room], so hopefully this goes well ;).
 */
@Database(
    entities = [Post::class, User::class, Album::class, Photo::class],
    views = [DisplayPost::class, PostDetails::class],
    version = 1,
    exportSchema = false
)
abstract class PostDatabase : RoomDatabase() {
    abstract fun postDao(): PostDao
}
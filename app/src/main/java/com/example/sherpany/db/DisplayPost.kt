package com.example.sherpany.db

import androidx.room.DatabaseView

typealias DisplayPosts = List<DisplayPost>

@DatabaseView(
    value = """SELECT posts.id, posts.title, users.email AS userEmail 
        FROM posts INNER JOIN users on posts.userId = users.id""",
    viewName = "displayPosts"
)
data class DisplayPost(
    val id: Long,
    val title: String,
    val userEmail: String
)
package com.example.sherpany.repo

import android.util.Log
import androidx.lifecycle.LiveData
import com.example.sherpany.api.PostService
import com.example.sherpany.common.onBackground
import com.example.sherpany.db.DisplayPosts
import com.example.sherpany.db.PostDao
import com.example.sherpany.model.Users
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class PostRepository @Inject constructor(
    private val service: PostService,
    private val postDao: PostDao
) {

    val posts: LiveData<DisplayPosts> = getDisplayPostsFromDb()

    fun deletePost(id: Long) {
        Completable.fromAction {
            postDao.deletePostById(id)
        }.onBackground().subscribe()
    }

    /* DB */

    private fun getDisplayPostsFromDb() = postDao.getDisplayPosts()

    fun getPostAlbums(postId: Long) = postDao.getPostAlbums(postId)

    fun getPostDetails(postId: Long) = postDao.getPostDetails(postId)

    /* API */

    private fun getUsersFromApi(): Observable<Users> = service.getUsers()
        .doOnNext { users -> postDao.insertUsers(users) }
        .doOnError { err -> Log.e(this::class.simpleName, "Unable to get Users from API", err) }

    private fun getPostsFromApi() = service.getPosts()
        .doOnNext { posts -> postDao.insertPosts(posts) }
        .doOnError { err -> Log.e(this::class.simpleName, "Unable to get Posts from API", err) }

    private fun getAlbumsFromApi() = service.getAlbums()
        .doOnNext { albums -> postDao.insertAlbums(albums) }
        .doOnError { err -> Log.e(this::class.simpleName, "Unable to get Albums from API", err) }

    private fun getPhotosFromApi() = service.getPhotos()
        .doOnNext { photos -> postDao.insertPhotos(photos) }
        .doOnError { err -> Log.e(this::class.simpleName, "Unable to get Photos from API", err) }

    /**
     * Fetch new posts from the API.
     */
    fun fetchPosts() {
        Observable.merge(
            getUsersFromApi(),
            getPostsFromApi(),
            getAlbumsFromApi(),
            getPhotosFromApi()
        ).onBackground().subscribe()
    }

}
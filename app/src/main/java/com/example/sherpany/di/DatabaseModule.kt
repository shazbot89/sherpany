package com.example.sherpany.di

import android.content.Context
import androidx.room.Room
import com.example.sherpany.db.PostDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun providePostDatabase(@ApplicationContext context: Context): PostDatabase =
        Room.databaseBuilder(context, PostDatabase::class.java, "postDb").build()

    @Provides
    @Singleton
    fun providePostDao(database: PostDatabase) = database.postDao()
}
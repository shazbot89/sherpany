package com.example.sherpany.bind

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.sherpany.R
import com.squareup.picasso.Picasso


@Suppress("UNCHECKED_CAST")
@BindingAdapter("data")
fun <T> RecyclerView.setData(data: T) {
    if (adapter is BindableAdapter<*>) {
        (adapter as BindableAdapter<T>).setData(data)
    }
}

interface BindableAdapter<T> {
    fun setData(data: T?)
}

@BindingAdapter("imageUrl")
fun setImageUrl(view: ImageView, imgUrl: String) = Picasso
    .get()
    .load(imgUrl)
    .placeholder(R.drawable.ic_image_placeholder)
    .into(view)
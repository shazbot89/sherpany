package com.example.sherpany.model

import androidx.room.Entity
import androidx.room.PrimaryKey

typealias Photos = List<Photo>

@Entity(tableName = "photos")
data class Photo(
    val albumId: Long,
    @PrimaryKey
    val id: Long,
    val title: String,
    val url: String,
    val thumbnailURL: String?
)

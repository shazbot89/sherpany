package com.example.sherpany.model

import androidx.room.Entity
import androidx.room.PrimaryKey

typealias Posts = List<Post>

@Entity(tableName = "posts")
data class Post(
    val userId: Long,
    @PrimaryKey
    val id: Long,
    val title: String,
    val body: String
)
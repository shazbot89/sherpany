package com.example.sherpany.model

import androidx.room.Entity
import androidx.room.PrimaryKey

typealias Albums = List<Album>

@Entity(tableName = "albums")
data class Album(
    val userId: Long,
    @PrimaryKey
    val id: Long,
    val title: String
)
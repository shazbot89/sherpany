package com.example.sherpany.api

import com.example.sherpany.model.Albums
import com.example.sherpany.model.Photos
import com.example.sherpany.model.Posts
import com.example.sherpany.model.Users
import io.reactivex.Observable
import retrofit2.http.GET


/**
 * Retrofit service for the Typicode API.
 */
interface PostService {

    @GET("posts")
    fun getPosts(): Observable<Posts>

    @GET("users")
    fun getUsers(): Observable<Users>

    @GET("albums")
    fun getAlbums(): Observable<Albums>

    @GET("photos")
    fun getPhotos(): Observable<Photos>
}
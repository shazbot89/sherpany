# Sherpany
![badge-ci]

## :new: Features
- [x] Set the navigation bar title to “Challenge Accepted!”
- [x] Use Kotlin
- [x] Fetch the data every time the app is cold started
- [x] Persist the data in DataBase with relationships
- [x] Merge fetched data with persisted data
- [x] Use fragments
- [x] Support phone / tablets (swp600)
- [x] Always visible, fixed width list view
- [x] The detail view adapts to the space available
- [x] Display list of posts
- [x] Display full post title and user email with variable height
- [x] Allow deletion of post
- [x] Selecting a post displays the detail to this post
- [x] Show post details: title, post body and related albums
- [x] Display albums in the post details
- [x] Load photos efficiently
### :sparkles: Bonus:
- [ ] It would be nice to be able to show/hide the photos of an album. All albums are collapsed in the default state
- [x] Because the collection of photos can get quite long, we would like the headers to stick to the top
- [x] Include a search bar to search in the master view and provide live feedback to the user while searching

## :tv: Videos

| :iphone: Phone | :black_square_button: Tablet |
|---|---|
| [![demo-phone]](http://www.youtube.com/watch?v=3QxRuvFMQqM) | [![demo-tablet]](http://www.youtube.com/watch?v=BxSnHNhR-yA) |

## :rocket: Getting Started

### Build
```bash
$ ./gradlew build
```

[demo-phone]: http://img.youtube.com/vi/3QxRuvFMQqM/0.jpg "Demo (Phone)"
[demo-tablet]: http://img.youtube.com/vi/BxSnHNhR-yA/0.jpg "Demo (Tablet)"
[badge-ci]: https://gitlab.com/shazbot89/sherpany/badges/master/pipeline.svg "Pipeline Status"
